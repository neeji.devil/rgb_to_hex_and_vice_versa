order = ['A','B','C','D','E','F']
valid = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','#']

red_l = list()
green_l = list()
blue_l = list()

#--------------------------------------------rgb_to_hex function--------------------------------------------------
def rgb_to_hex(red,green,blue):
	global red_l
	global green_l
	global blue_l
	if red < 256 and green < 256 and blue < 256 and red > -1 and green > -1 and blue > -1:
		while red > 0:
			red_l.append(red%16)
			red = red // 16
		while green > 0:
			green_l.append(green%16)
			green = green // 16
		while blue > 0:
			blue_l.append(blue%16)
			blue = blue // 16
	else:
		print("the values entered are not correct please check again")
		exit(1)
	ordering()

#----------------------------end of rgb_to_hex function -----------------------------------------------------------


#-----------------------------ordering function--------------------------------------------------------------------
def ordering():
	global red_l
	global green_l
	global blue_l
	global order
	
	red_l.reverse()
	green_l.reverse()
	blue_l.reverse()

	red_t = list()
	green_t = list()
	blue_t = list()
	
	for number in red_l:
		if number > 9 :
			red_t.append( str(order[number%9 - 1]) )
		else:
			red_t.append(number)

	for number in green_l:
		if number > 9:
			green_t.append( str(order[number%9 - 1]) )
		else:
			green_t.append(number)

	for number in blue_l:
		if number > 9:
			blue_t.append( str(order[number%9 - 1]) )
		else:
			blue_t.append(number)

#	print('red list contains {0}\ngreen list contains {1}\nblue list contains {2}'.format(red_t,green_t,blue_t))
	red_l = red_t
	green_l = green_t
	blue_l = blue_t


#--------------------------------------------end of ordering function ----------------------------------------------


#-----------------------------------------hex_to_rgb function-------------------------------------------------------

def hex_to_rgb(hex1):
	red = ''
	green = ''
	blue = ''
	if hex1[0] == '#':
		red = hex1[1:3]
		green = hex1[3:5]
		blue = hex1[5:7]
	else:
		red = hex1[0:2]
		green = hex1[2:4]
		blue = hex1[4:6]
#	print(red)
#	print(blue)
#	print(green)

	red_l = list()
	red_l.append(red[0])
	red_l.append(red[1])

	green_l = list()
	green_l.append(green[0])
	green_l.append(green[1])

	blue_l = list()
	blue_l.append(blue[0])
	blue_l.append(blue[1])


#	print(red_l)
#	print(green_l)
#	print(blue_l)

	if red_l[0].isdigit():
		red_l.append(int(red_l[0]))
	else:
		ind = order.index(red_l[0])
		re = 9 + 1 + ind
		red_l.append(re)

	if red_l[1].isdigit():
		red_l.append(int(red_l[1]))
	else:
		ind = order.index(red_l[1])
		re = 9 + 1 + ind
		red_l.append(re)


	if green_l[0].isdigit():
		green_l.append(int(green_l[0]))
	else:
		ind = order.index(green_l[0])
		gre = 9 + 1 + ind
		green_l.append(gre)

	if green_l[1].isdigit():
		green_l.append(int(green_l[1]))
	else:
		ind = order.index(green_l[1])
		gre = 9 + 1 + ind
		green_l.append(gre)


	if blue_l[0].isdigit():
		blue_l.append(int(blue_l[0]))
	else:
		ind = order.index(blue_l[0])
		bl = 9 + 1 + ind
		blue_l.append(bl)

	if blue_l[1].isdigit():
		blue_l.append(int(blue_l[1]))
	else:
		ind = order.index(blue_l[1])
		bl = 9 + 1 + ind
		blue_l.append(bl)

#	print(red_l)
#	print(green_l)
#	print(blue_l)

	red = 16*int(red_l[2]) + int(red_l[3])
	green = 16*int(green_l[2]) + int(green_l[3])
	blue = 16*int(blue_l[2]) + int(blue_l[3])

	return(red,green,blue)

#----------------------------------------end of hex-to-rgb function-----------------------------------------------


#----------------------function calls and main data---------------------------------------------------------------

print("1. for converting rgb to hex.")
print("2. for converting hex to rgb.")
choice = input("Enter your choice:")

if choice == "1":
	red = int(input("Red : "))
	green = int(input("Green : "))
	blue = int(input("Blue : "))

	rgb_to_hex(red,green,blue)
	#print('red list contains {0}\ngreen list contains {1}\nblue list contains {2}'.format(red_l,green_l,blue_l))

	hex1 = '#'
	for number in red_l:
		hex1 = hex1 + str(number)
	for number in green_l:
		hex1 = hex1 + str(number)
	for number in blue_l:
		hex1 = hex1 + str(number)

	print('the hex for the following value is {0}'.format(hex1))

else:
	if choice == "2":
		hex1 = input("Enter the hex value starting from # or number but not other than that")
		for i in hex1:
			if i not in valid :
				print(i)
				print("incorrect hex entered.")
				exit(1)

		(red,green,blue) = hex_to_rgb(hex1)
		print('Red {0}'.format(red))
		print('Green {0}'.format(green))
		print('Blue {0}'.format(blue))

	else:
		print("No such option provided")